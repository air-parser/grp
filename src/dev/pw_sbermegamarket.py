import asyncio
import time

from playwright.async_api import async_playwright
from playwright.async_api import Page
from playwright_stealth import stealth_async
from src.help.tracking import tracking_request, tracking_response

async def get_collection(page: Page, category: str = '0'):

    async with page.expect_response(lambda response: response.url == 'https://sbermegamarket.ru/api/mobile/v1/catalogService/catalog/menu' and response.status == 200, timeout = 2 * 60 * 1000) as response:
        response = await response.value
        data = await response.json()
        for category in list(filter(lambda d: d['parentId'] != category, data.get('nodes'))):
            collection = category.get('collection')
            yield collection


async def main():
    async with async_playwright() as p:
        browser = await p.firefox.launch(
            headless = False, slow_mo = 500,
        )
        context = await browser.new_context(
            base_url = "https://sbermegamarket.ru/",
            proxy = {
                "server": "http://193.41.39.132:50100",
                "username": "mskonline",
                "password": "diENBCkwGh"
            }
            # proxy = {
            #     "server":
            #     "username":
            #     "password":
            # }
        )
        page = await context.new_page()
        await stealth_async(page)
        await page.goto('http://checker.soax.com/api/ipinfo')
        print(await page.content())
        page.on("request", tracking_request)
        page.on("response", tracking_response)
        api_request_context = context.request
        await page.goto("https://sbermegamarket.ru/", timeout = 1000 * 60 * 2)
        async for collection in get_collection(page):
            response = await api_request_context.post(
                "https://sbermegamarket.ru/api/mobile/v1/catalogService/catalog/search",
                data = {
                    "requestVersion": 10,
                    "limit": 44,
                    "offset": 0,
                    "collectionId": collection.get('collectionId'),
                    "selectedAssumedCollectionId": '',
                    "isMultiCategorySearch": False,
                    "searchByOriginalQuery": False,
                    "selectedSuggestParams": [],
                    "sorting": 0,
                    "ageMore18": 2,
                    "showNotAvailable": True,
                }
            )
            print(await response.json())

        await browser.close()

asyncio.run(main())
# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter


from crawlab.result import save_item
from crawlab.config import get_task_id
from crawlab.entity.result import Result


class CrawlabMongoPipeline(object):

    def process_item(self, item, spider):
        result = Result(item)
        result.set_task_id(get_task_id())
        save_item(result)
        return item

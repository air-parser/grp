import time

import scrapy
import orjson
import logging
from scrapy.exceptions import CloseSpider
from playwright_stealth import stealth_async

logger = logging.getLogger('scrapy')


async def init_page(page, request):
    time.sleep(20)


class SbermegamarketSpider(scrapy.Spider):
    name = "sbermegamarket"
    allowed_domains = ["sbermegamarket.ru"]

    custom_settings = {
        'HTTPERROR_ALLOW_ALL': True,
        'DOWNLOAD_HANDLERS': {
            "http": "scrapy_playwright.handler.ScrapyPlaywrightDownloadHandler",
            "https": "scrapy_playwright.handler.ScrapyPlaywrightDownloadHandler"
        },
        'CONCURRENT_REQUESTS_PER_DOMAIN': 1,
        'PLAYWRIGHT_BROWSER_TYPE': "firefox",
        #'LOG_LEVEL': 'INFO',
        'LOGSTATS_INTERVAL': 5.0,
        'CLOSESPIDER_TIMEOUT': 5000,

        "PLAYWRIGHT_LAUNCH_OPTIONS": {
            "headless": False,
            # "proxy": {
            #     "server":
            #     "username":
            #     "password":
            # }

        }
    }

    def start_requests(self):
        yield scrapy.Request(
            "http://sbermegamarket.ru/",
            meta = {
                "playwright": True,
                "playwright_include_page": True,
                # "playwright_page_init_callback": init_page,
            },
            callback = self.main_catalog,
            errback = self.errback_close_page,
        )

    async def main_catalog(self, response, category: str = '0'):
        page = response.meta["playwright_page"]
        await stealth_async(page)
        print(await page.content())

        async with page.expect_response(lambda response: response.url == 'https://sbermegamarket.ru/api/mobile/v1/catalogService/catalog/menu' and response.status == 200, timeout = 5 * 1000) as resp:
            resp = await resp.value
            data = await resp.json()
            for cat in list(filter(lambda d: d['parentId'] != category, data.get('nodes')))[13:14]:
                collection = cat.get('collection')
                request_body = orjson.dumps({
                    "requestVersion": 10,
                    "limit": 44,
                    "offset": 0,
                    "collectionId": collection.get('collectionId'),
                    "selectedAssumedCollectionId": '',
                    "isMultiCategorySearch": False,
                    "searchByOriginalQuery": False,
                    "selectedSuggestParams": [],
                    "sorting": 0,
                    "ageMore18": 2,
                    "showNotAvailable": True,
                })

                yield scrapy.Request(
                    "https://sbermegamarket.ru/api/mobile/v1/catalogService/catalog/search",
                    method = "POST",
                    body = request_body,
                    callback = self.get_items,
                    cb_kwargs = {'collection': collection},
                )
        await page.close()

    async def get_items(self, response, collection, limit: int = 44, offset: int = 44):
        data = orjson.loads(response.text)

        for item in data.get('items'):
            yield item

        total = int(data.get('total'))
        log_str = '{} из {}'.format(offset, total)
        logger.info(log_str)

        if offset > total:
            raise CloseSpider('bandwidth_exceeded')

        offset += 44
        request_body = orjson.dumps({
            "requestVersion": 10,
            "limit": limit,
            "offset": offset,
            "collectionId": collection.get('collectionId'),
            "selectedAssumedCollectionId": '',
            "isMultiCategorySearch": False,
            "searchByOriginalQuery": False,
            "selectedSuggestParams": [],
            "sorting": 0,
            "ageMore18": 2,
            "showNotAvailable": True,
        })

        yield scrapy.Request(
            "https://sbermegamarket.ru/api/mobile/v1/catalogService/catalog/search",
            method="POST",
            body=request_body,
            callback=self.get_items,
            cb_kwargs = {'collection': collection, 'offset': offset},
        )

    async def errback_close_page(self, failure):
        page = failure.request.meta["playwright_page"]
        await page.close()

import time

import scrapy
from help import tracking


async def init_page(page, request):
    page.on("request", tracking.tracking_request)
    async with page.expect_response(lambda response: response.url == "https://vitaexpress.ru/catalog/" and response.status == 200):
        time.sleep(10)

class VitaexpressSpider(scrapy.Spider):
    name = "vitaexpress"
    allowed_domains = ["vitaexpress.ru"]

    custom_settings = {
        'HTTPERROR_ALLOW_ALL': True,
        'DOWNLOAD_HANDLERS': {
            "http": "scrapy_playwright.handler.ScrapyPlaywrightDownloadHandler",
            "https": "scrapy_playwright.handler.ScrapyPlaywrightDownloadHandler"
        },
        'CONCURRENT_REQUESTS_PER_DOMAIN': 1,
        'PLAYWRIGHT_BROWSER_TYPE': "firefox",
        # 'LOG_LEVEL': 'INFO',
        'LOGSTATS_INTERVAL': 5.0,
        'CLOSESPIDER_TIMEOUT': 5000,
        "PLAYWRIGHT_MAX_PAGES_PER_CONTEXT": 1,
        "PLAYWRIGHT_LAUNCH_OPTIONS": {
            "headless": False,
            "slow_mo": 2000
            # 'proxy': {
            #     "server": "http://@proxy.soax.com:9000",
            #     "username": "FuZxst7BQAxwhDzG",
            #     "password": "wifi;ru"
            # }
        }
    }

    def start_requests(self):
        yield scrapy.Request(
            "http://checker.soax.com/api/ipinfo",
            meta = {
                "playwright": True,
                #"playwright_page_init_callback": init_page,
                "playwright_include_page": True,
            },
            callback = self.category
        )

    async def category(self, response):
        page = response.meta["playwright_page"]
        await page.goto('https://vitaexpress.ru/catalog/')
        time.sleep(10)
        selector = scrapy.Selector(text = await page.content())
        categories = selector.xpath('.//a[@class="subCat__link"]/@href').getall()
        for category in categories:
            url = 'https://vitaexpress.ru' + category
            if url == 'https://vitaexpress.ru/catalog/lekarstva-i-bady/lekarstva-ot-prostudy/':
                print('------- SCROLL ---------')
                await page.goto(url)
                for _ in range(30):
                    await page.mouse.wheel(0, 15000)
                    time.sleep(0.5)
            # if url == 'https://vitaexpress.ru/catalog/lekarstva-i-bady/lekarstva-ot-prostudy/':
            #     print()

            # yield scrapy.Request(
            #     url = 'https://vitaexpress.ru' + category,
            #     meta = {
            #         "playwright": True,
            #         "playwright_page": page,
            #         "playwright_include_page": True,
            #     },
            #     callback = self.parse_category
            # )

    async def parse_category(self, response):
        page = response.meta["playwright_page"]
        print(page)
        time.sleep(0.5)
        # products = response.xpath(".//div[@class='b-product__wrap']/a/@href")
        # print(len(products))
        #
        # # for product in products:
        # #     print(product)
        # next_page = response.xpath('//a[contains(@class, "pagination__nav-item--next")]/@href').get()
        # if next_page:
        #     yield response.follow(next_page, self.parse_catego
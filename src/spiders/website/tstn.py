import time
import scrapy
from help.tracking import tracking_request, tracking_response


async def init_page(page, request):
    page.on("request", tracking_request)
    page.on("response", tracking_response)
    async with page.expect_response(lambda response: "https://www.tstn.ru/__qrator/validate" in response.url and response.status == 200):
        print(' -- fuck off qrator -- ')
        time.sleep(5)


class TstnSpider(scrapy.Spider):
    name = "tstn"
    allowed_domains = ["tstn.ru"]

    ITEM_PIPELINES = {
        'core.pipelines.CrawlabMongoPipeline': 888,
    }

    custom_settings = {
        'HTTPERROR_ALLOW_ALL': True,
        'DOWNLOAD_HANDLERS': {
            "http": "scrapy_playwright.handler.ScrapyPlaywrightDownloadHandler",
            "https": "scrapy_playwright.handler.ScrapyPlaywrightDownloadHandler"
        },
        'DOWNLOAD_DELAY': 5,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 1,
        'PLAYWRIGHT_BROWSER_TYPE': "firefox",
        #'LOG_LEVEL': 'INFO',
        'LOGSTATS_INTERVAL': 5.0,
        'CLOSESPIDER_TIMEOUT': 5000,
        #'ITEM_PIPELINES': ITEM_PIPELINES,
        "PLAYWRIGHT_LAUNCH_OPTIONS": {
            "headless": False,
        }
    }

    def start_requests(self):
        yield scrapy.Request(
            "https://www.tstn.ru/shop/",
            meta={
                "playwright": True,
                # "playwright_page_init_callback": init_page,
                "playwright_include_page": True,
            },
            callback=self.set_cookie
        )

    async def set_cookie(self, response):
        page = response.meta["playwright_page"]
        time.sleep(20)
        cookies = {
            cookie['name']: cookie['value']
            for cookie in await page.context.cookies()
        }
        print('------------', cookies)
        await page.close()
        yield scrapy.Request(
            'https://www.tstn.ru/shop/',
            cookies = cookies,
            callback = self.catalog,
            dont_filter = True,
        )

    async def catalog(self, response):
        print(response.text)



    # custom_settings = {
    #     'CONCURRENT_REQUESTS_PER_DOMAIN': 1,
    #     'PLAYWRIGHT_BROWSER_TYPE': "firefox",
    #     "RETRY_TIMES": 0,
    # }

    # def start_requests(self):
    #     yield scrapy.Request(
    #         'http://checker.soax.com/api/ipinfo',
    #         callback = self.parse
    #     )
    #
    # async def parse(self, response):
    #
    #     async with async_playwright() as p:
    #         browser = await p.firefox.launch(headless=False, slow_mo=200)
    #         context = await browser.new_context()
    #         await stealth_async(context)
    #         page = await context.new_page()
    #         await page.goto("https://www.tstn.ru/shop/")
    #         await asyncio.sleep(10)
    #         cookies = {
    #             cookie['name']: cookie['value']
    #             for cookie in await page.context.cookies()
    #         }
    #         await browser.close()
    #         yield scrapy.Request(
    #             url = "https://www.tstn.ru/shop/",
    #             cookies=cookies,
    #             callback = self.set_cookie
    #         )
    #
    # async def set_cookie(self, response):
    #     print(response)
        # page = response.meta["playwright_page"]
        # cookies = {
        #     cookie['name']: cookie['value']
        #     for cookie in await page.context.cookies()
        # }
        # print(response)
        # print('----------------------', cookies)

        # yield scrapy.Request(
        #     url = 'https://www.tstn.ru/shop/',
        #     cookies = cookies,
        #     # meta = {
        #     #     "playwright": True,
        #     #     "playwright_include_page": True,
        #     #     "playwright_page": page,
        #     # },
        #     callback = self.catalog
        # )

    # async def catalog(self, response):
    #     #page = response.meta["playwright_page"]
    #     categories = response.xpath('.//li[@class="catalog-category__item"]/a/@href').getall()
    #     for category in categories:
    #         url = 'https://www.tstn.ru/shop/' + category
    #         print(url)
    #         yield scrapy.Request(url, callback = self.parse_category,)
    #
    # async def parse_category(self, response):
    #     page = response.meta["playwright_page"]
    #     print('---- OK -------')
    #
    #     # content = await page.content()
    #     # print(content)
    #     # # next_page = response.xpath('//a[contains(@class, "pagination__nav-item--next")]/@href').get()
    #     # # if next_page:
    #     # #     yield response.follow(next_page, self.parse_category)
    #     # #
    #     # products = response.xpath(".//div[@class='b-product__wrap']/a/@href")
    #     # for product in products:
    #     #     print(product.get())
    #     # #     yield response.follow(product.get(), self.parse_detail)
    #
    # async def parse_detail(self, response):
    #     print(response.text)
    #     item = {
    #         'url': response.url
    #     }
    #     yield item

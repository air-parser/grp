import orjson
import math
import scrapy
from urllib.parse import urlencode
from scrapy.http import JsonRequest
from help import tracking


async def init_page(page, request):
    page.on("request", tracking.tracking_request)
    async with page.expect_request(lambda request: "https://www.auchan.ru/scripts/runtime.2957.js" in request.url, timeout= 5 * 1000):
        pass


class AuchanSpider(scrapy.Spider):
    name = "auchan"
    allowed_domains = ["auchan.ru"]

    ITEM_PIPELINES = {
        'core.pipelines.CrawlabMongoPipeline': 888,
    }

    custom_settings = {
        'HTTPERROR_ALLOW_ALL': True,
        'DOWNLOAD_HANDLERS': {
            "http": "scrapy_playwright.handler.ScrapyPlaywrightDownloadHandler",
            "https": "scrapy_playwright.handler.ScrapyPlaywrightDownloadHandler"
        },
        'CONCURRENT_REQUESTS_PER_DOMAIN': 5,
        'PLAYWRIGHT_BROWSER_TYPE': "firefox",
        'LOG_LEVEL': 'INFO',
        'LOGSTATS_INTERVAL': 5.0,
        'CLOSESPIDER_TIMEOUT': 5000,
        'ITEM_PIPELINES': ITEM_PIPELINES
    }

    def start_requests(self):
        yield scrapy.Request(
            "http://auchan.ru/",
            meta = {
                "playwright": True,
                "playwright_include_page": True,
            },
            callback = self.set_cookie
        )

    async def set_cookie(self, response):
        page = response.meta["playwright_page"]

        cookies = {
            cookie['name']: cookie['value']
            for cookie in await page.context.cookies()
        }
        await page.close()
        yield scrapy.Request(
            'https://www.auchan.ru/catalog/', cookies = cookies, callback = self.catalog
        )

    async def catalog(self, response):
        data = response.xpath('//script[@id="init"]/text()').get()
        data = data.replace("window.__INITIAL_STATE__ = ", '')

        data = orjson.loads(data)

        for category in data.get('categories', {}).get('categories', {}):
            code = category.get('code')
            total = category.get('productsCount')

            for num_page in range(1, math.ceil(total / 100) + 1):
                params = {
                    'merchantId': '1',
                    'page': num_page,
                    'perPage': '100',
                }
                data = {
                    'filter': {
                        'category': code,
                        'promo_only': False,
                        'active_only': False,
                        'cashback_only': False
                    }
                }
                yield JsonRequest(
                    'https://www.auchan.ru/v1/catalog/products/?' + urlencode(params),
                    data = data,
                    callback = self.products,
                    cb_kwargs = {'category': category.get('name')}
                )

    async def products(self, response, category):
        data = orjson.loads(response.text)
        items = data.get('items')

        for item in items:
            yield item


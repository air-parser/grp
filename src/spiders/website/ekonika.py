import scrapy
from urllib.parse import urlencode



class EkonikaSpider(scrapy.Spider):
    name = "ekonika"
    allowed_domains = ["ekonika.ru"]

    ITEM_PIPELINES = {
        'core.pipelines.CrawlabMongoPipeline': 888,
    }
    custom_settings = {
        'HTTPERROR_ALLOW_ALL': True,
        'DOWNLOAD_HANDLERS': {
            "http": "scrapy_playwright.handler.ScrapyPlaywrightDownloadHandler",
            "https": "scrapy_playwright.handler.ScrapyPlaywrightDownloadHandler"
        },
        'CONCURRENT_REQUESTS_PER_DOMAIN': 1,
        'PLAYWRIGHT_BROWSER_TYPE': "firefox",
        'LOG_LEVEL': 'INFO',
        'LOGSTATS_INTERVAL': 5.0,
        'CLOSESPIDER_TIMEOUT': 5000,
        'ITEM_PIPELINES': ITEM_PIPELINES
    }

    def start_requests(self):
        yield scrapy.Request(
            "http://ekonika.ru/",
            meta = {
                "playwright": True,
                "playwright_include_page": True,
            },
            callback = self.set_cookie
        )

    async def set_cookie(self, response):
        page = response.meta["playwright_page"]
        cookies = {
            cookie['name']: cookie['value']
            for cookie in await page.context.cookies()
        }
        await page.close()

        yield scrapy.Request(
            'https://ekonika.ru/catalog', cookies = cookies, callback = self.parse_catalog
        )

    async def parse_catalog(self, response):
        catalog = response.xpath('.//li[@class="catalog-menu__item"]/a/@href').getall()
        categories = [x for x in catalog if 'catalog' in x]
        for category in categories:
            yield response.follow(category, self.parse_category)

    async def parse_category(self, response):
        products = response.xpath("//div/@data-id/parent::*")
        for product in products:
            params = {
                'site': 's1',
                'code': product.xpath('./@data-product-id').get(),
                'id': product.xpath('./@data-id').get(),
            }
            yield scrapy.Request(
                url = 'https://ekonika.ru/ajax/product_preview.php/?' + urlencode(params),
                callback = self.detail_parse,
            )

        next_page = response.xpath('//a[contains(@class, "paging-next")]/@href').get()
        if next_page:
            yield response.follow(next_page, self.parse_category)

    async def detail_parse(self, response):
        item = {
            'url': response.url
        }
        yield item

import logging
from playwright.async_api import Request, Response

logger = logging.getLogger('scrapy')


async def tracking_request(request: Request):
    log_str = '{} - {}'.format(request.method, request.url)
    print('# Request - ', log_str, '#######')

async def tracking_response(response: Response):
    log_str = '{} - {}'.format(response.status, response.url)
    print('# Response - ', log_str, '')
